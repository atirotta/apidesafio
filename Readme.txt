

**************DESAFIO SUPERVIELLE*****************
*DESARROLLADOR: AUGUSTO TIROTTA                  *
**************************************************


-Para desarrollar la API se usó principalmente Java 8, Spring Boot, Hibernate, JPA. 
 La base de datos se crea al momento de iniciar la aplicación usando H2 Database Engine, con lo cual no se debe tener en cuenta nada mas.
 
-Documentacion realizada con la tecnologia de Swagger, tan solo ingresar a https://apidesafiotirotta.herokuapp.com/swagger-ui.html# y tendremos la info para realizar los test necesarios.

-Api publicada en Heroku.

-Importar el codigo y ejecutarlo localmente:
    -Importar el proyecto como maven.
	-Compilar proyecto maven -> mvn clean install
	-Para ejecutar los test tener en cuenta de realizarlo con JUnit 4.


********API REPOSITORY****************
-https://bitbucket.org/atirotta/apidesafio

********API PUBLICADA HEROKU****************
-Link prueba API: https://apidesafiotirotta.herokuapp.com/swagger-ui.html#


********API LOCAL **************************
-Link prueba API: http://localhost:8080/swagger-ui.html#
-Link Acceso a base de datos: http://localhost:8080/h2-console



********Condiciones relevantes a tener en cuenta en el body de los request*******
-El sexo debe ser 'M' o 'F'
-Pais: nombre del pais, ej: argentina


