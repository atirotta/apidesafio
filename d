[1mdiff --git a/src/main/java/com/api/desafio/controller/PersonaController.java b/src/main/java/com/api/desafio/controller/PersonaController.java[m
[1mindex d02f19c..ccadd03 100644[m
[1m--- a/src/main/java/com/api/desafio/controller/PersonaController.java[m
[1m+++ b/src/main/java/com/api/desafio/controller/PersonaController.java[m
[36m@@ -63,8 +63,8 @@[m [mpublic class PersonaController {[m
 		return new ResponseEntity<String>(HttpStatus.NO_CONTENT);[m
     }[m
 	[m
[31m-	@PatchMapping[m
[31m-	@ApiOperation(value = "/{id}", notes = "Actualiza los campos completados de una persona en la base de datos")[m
[32m+[m	[32m@PatchMapping(value = "/{id}")[m
[32m+[m	[32m@ApiOperation(value = "Actualiza la persona", notes = "Actualiza los campos completados de una persona en la base de datos")[m
     public ResponseEntity<String> updatePersona(@Valid @RequestBody PersonaUpdateDto persona, @PathVariable("id") Long personaId) {[m
 		this.personaService.updatePersona(persona,personaId);[m
 		return new ResponseEntity<String>(HttpStatus.OK);[m
[1mdiff --git a/src/main/java/com/api/desafio/service/PersonaService.java b/src/main/java/com/api/desafio/service/PersonaService.java[m
[1mindex d90eee6..f484013 100644[m
[1m--- a/src/main/java/com/api/desafio/service/PersonaService.java[m
[1m+++ b/src/main/java/com/api/desafio/service/PersonaService.java[m
[36m@@ -30,7 +30,7 @@[m [mpublic class PersonaService {[m
 	[m
 	public List<PersonaGet> getPersonas() {[m
 		Iterable<Persona> personas = this.personaRepository.findAll();[m
[31m-		if(personas == null ) {[m
[32m+[m		[32mif(personas == null || ((List<Persona>) personas).isEmpty()) {[m
 			throw new PersonasNoExistentesException();[m
 		}[m
 		[m
[36m@@ -75,6 +75,10 @@[m [mpublic class PersonaService {[m
 [m
 	@Transactional[m
 	public void deletePerson(Long personaId) {[m
[32m+[m		[32mOptional<Persona> persona = this.personaRepository.findById(personaId);[m
[32m+[m		[32mif(!persona.isPresent()) {[m
[32m+[m			[32mthrow new PersonaNoEncontradaException();[m
[32m+[m		[32m}[m
 		this.personaRepository.deleteById(personaId);[m
 	}[m
 [m
[1mdiff --git a/src/test/java/com/api/desafio/test/PersonaServiceTest.java b/src/test/java/com/api/desafio/test/PersonaServiceTest.java[m
[1mindex f8e83c2..475025b 100644[m
[1m--- a/src/test/java/com/api/desafio/test/PersonaServiceTest.java[m
[1m+++ b/src/test/java/com/api/desafio/test/PersonaServiceTest.java[m
[36m@@ -7,9 +7,7 @@[m [mimport java.util.List;[m
 import java.util.Optional;[m
 [m
 import org.junit.Before;[m
[31m-import org.junit.Rule;[m
[31m-import org.junit.jupiter.api.Test;[m
[31m-import org.junit.rules.ExpectedException;[m
[32m+[m[32mimport org.junit.Test;[m
 import org.junit.runner.RunWith;[m
 import org.mockito.Mockito;[m
 import org.mockito.MockitoAnnotations;[m
[36m@@ -21,6 +19,9 @@[m [mimport org.springframework.test.context.junit4.SpringRunner;[m
 import com.api.desafio.dto.PersonaCreateDto;[m
 import com.api.desafio.dto.PersonaGet;[m
 import com.api.desafio.entity.Persona;[m
[32m+[m[32mimport com.api.desafio.exceptions.PersonaDatosContactoVacioException;[m
[32m+[m[32mimport com.api.desafio.exceptions.PersonaNoEncontradaException;[m
[32m+[m[32mimport com.api.desafio.exceptions.PersonasNoExistentesException;[m
 import com.api.desafio.repository.PersonaRepository;[m
 import com.api.desafio.service.PersonaService;[m
 [m
[36m@@ -33,36 +34,38 @@[m [mpublic class PersonaServiceTest {[m
 	@MockBean[m
 	PersonaRepository personaRepositoryMock;[m
 	[m
[32m+[m	[32mPersona josePerez ;[m
[32m+[m	[32mPersona pabloPerez ;[m
[32m+[m	[32mPersona padrePerez;[m
[32m+[m	[32mPersona madrePerez;[m[41m [m
[32m+[m	[32mList<Persona> listPersonas;[m
[32m+[m[41m	[m
 	@Before[m
 	public void setUp() {[m
 		MockitoAnnotations.initMocks(this);[m
[32m+[m		[32mPersona josePerez = new Persona(new Long(1),"1111111","argentina","dni","M","Jose","Perez", 20,"calle","avenida","4212-2323","1122334455");[m
[32m+[m		[32mPersona pabloPerez = new Persona(new Long(2),"2222222","argentina","dni","M","Pablo","Perez", 24,"calle","avenida","4212-2323","1122334456");[m
[32m+[m		[32mPersona padrePerez = new Persona(new Long(2),"2222222","argentina","dni","M","Raul","Perez", 24,"calle","avenida","4212-2323","1122334457");[m
[32m+[m		[32mPersona madrePerez = new Persona(new Long(2),"2222222","bolivia","dni","F","Mariz","Perez", 24,"calle","avenida","4212-2323","1122334458");[m
 		[m
[31m-		[m
[32m+[m		[32mlistPersonas = new ArrayList<Persona>();[m
[32m+[m		[32mlistPersonas.add(josePerez);[m
[32m+[m		[32mlistPersonas.add(pabloPerez);[m
[32m+[m		[32mlistPersonas.add(padrePerez);[m
[32m+[m		[32mlistPersonas.add(madrePerez);[m
 	}[m
 	[m
[31m-	@Rule[m
[31m-	public ExpectedException thrown = ExpectedException.none();[m
 	[m
[31m-	@Test[m
[31m-	public void cuandoObtengoPersonasEnAppNuevaDevuelveListaVacia() {[m
[32m+[m	[32m@Test(expected = PersonasNoExistentesException.class)[m
[32m+[m	[32mpublic void cuandoObtengoPersonasEnAppNuevaDevuelveExcepcion() {[m
 		Iterable<Persona> listVacia = new ArrayList<Persona>();[m
 		Mockito.when(personaRepositoryMock.findAll()).thenReturn(listVacia);[m
[31m-		[m
[31m-		assertTrue(personaService.getPersonas().isEmpty());[m
[31m-		[m
[32m+[m		[32mpersonaService.getPersonas();[m
 	}[m
 	[m
 	@Test[m
 	public void cuandoBuscoPersonasDadasDeAltaDevuelvenLaMismaCantidad() {[m
[31m-		Persona josePerez = new Persona(new Long(1),"1111111","argentina","dni","M","Jose","Perez", 20,"calle","avenida","4212-2323","1122334455");[m
[31m-		Persona pabloPerez = new Persona(new Long(2),"2222222","argentina","dni","M","Pablo","Perez", 24,"calle","avenida","4212-2323","1122334456");[m
[31m-		Persona padrePerez = new Persona(new Long(2),"2222222","argentina","dni","M","Raul","Perez", 24,"calle","avenida","4212-2323","1122334457");[m
[31m-		Persona madrePerez = new Persona(new Long(2),"2222222","bolivia","dni","F","Mariz","Perez", 24,"calle","avenida","4212-2323","1122334458");[m
[31m-		List<Persona> listPersonas = new ArrayList<Persona>();[m
[31m-		listPersonas.add(josePerez);[m
[31m-		listPersonas.add(pabloPerez);[m
[31m-		listPersonas.add(padrePerez);[m
[31m-		listPersonas.add(madrePerez);[m
[32m+[m[41m		[m
 		Mockito.when(personaRepositoryMock.findAll()).thenReturn((Iterable<Persona>)listPersonas);[m
 		[m
 		List<PersonaGet> personas = personaService.getPersonas();[m
[36m@@ -81,6 +84,17 @@[m [mpublic class PersonaServiceTest {[m
 		[m
 	}[m
 	[m
[32m+[m	[32m@Test(expected = PersonaNoEncontradaException.class)[m
[32m+[m	[32mpublic void cuandoBuscoPersonaNoExistenteLanzaExcepcion() {[m
[32m+[m		[32mpersonaService.getPersona(new Long(10));[m
[32m+[m	[32m}[m
[32m+[m[41m	[m
[32m+[m	[32m@Test(expected = PersonaDatosContactoVacioException.class)[m
[32m+[m	[32mpublic void cuandoCreoUnaPersonaSinDatosDeContactoLanzaExcepcion() {[m
[32m+[m		[32mPersonaCreateDto josePerezCreate = new PersonaCreateDto("1111111", "argentina", "dni", "M", "Jose", "Perez", 20, "", "", "", "");[m
[32m+[m		[32mpersonaService.createPerson(josePerezCreate);[m
[32m+[m	[32m}[m
[32m+[m[41m	[m
 	@Test[m
 	public void cuandoCreoUnaPersonaValidaSeDioDeAltaLaMisma() {[m
 		PersonaCreateDto josePerezCreate = new PersonaCreateDto("1111111", "argentina", "dni", "M", "Jose", "Perez", 20, "calle", "3avenida2", "4212-2323", "1122334455");[m
[36m@@ -92,6 +106,10 @@[m [mpublic class PersonaServiceTest {[m
 		assertTrue(personaService.getPersona(new Long(0)).getDocumento().equals(opcJosePerez.get().getDocumento()));[m
 	}[m
 	[m
[32m+[m	[32m@Test(expected = PersonaNoEncontradaException.class)[m
[32m+[m	[32mpublic void cuandoBorroUnaPersonaInexistenteLanzaExcepcion() {[m
[32m+[m		[32mpersonaService.deletePerson(new Long(50));[m
[32m+[m	[32m}[m
 	[m
 	[m
 }[m
