package com.api.desafio.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.api.desafio.dto.PersonaCreateDto;
import com.api.desafio.dto.PersonaGet;
import com.api.desafio.entity.Persona;
import com.api.desafio.exceptions.PersonaDatosContactoVacioException;
import com.api.desafio.exceptions.PersonaNoEncontradaException;
import com.api.desafio.exceptions.PersonasNoExistentesException;
import com.api.desafio.repository.PersonaRepository;
import com.api.desafio.service.PersonaService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonaServiceTest {

	@Autowired
	PersonaService personaService;
	@MockBean
	PersonaRepository personaRepositoryMock;
	
	Persona josePerez ;
	Persona pabloPerez ;
	Persona padrePerez;
	Persona madrePerez; 
	List<Persona> listPersonas;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Persona josePerez = new Persona(new Long(1),"1111111","argentina","dni","M","Jose","Perez", 20,"calle","avenida","4212-2323","1122334455");
		Persona pabloPerez = new Persona(new Long(2),"2222222","argentina","dni","M","Pablo","Perez", 24,"calle","avenida","4212-2323","1122334456");
		Persona padrePerez = new Persona(new Long(2),"2222222","argentina","dni","M","Raul","Perez", 24,"calle","avenida","4212-2323","1122334457");
		Persona madrePerez = new Persona(new Long(2),"2222222","bolivia","dni","F","Mariz","Perez", 24,"calle","avenida","4212-2323","1122334458");
		
		listPersonas = new ArrayList<Persona>();
		listPersonas.add(josePerez);
		listPersonas.add(pabloPerez);
		listPersonas.add(padrePerez);
		listPersonas.add(madrePerez);
	}
	
	
	@Test(expected = PersonasNoExistentesException.class)
	public void cuandoObtengoPersonasEnAppNuevaDevuelveExcepcion() {
		Iterable<Persona> listVacia = new ArrayList<Persona>();
		Mockito.when(personaRepositoryMock.findAll()).thenReturn(listVacia);
		personaService.getPersonas();
	}
	
	@Test
	public void cuandoBuscoPersonasDadasDeAltaDevuelvenLaMismaCantidad() {
		
		Mockito.when(personaRepositoryMock.findAll()).thenReturn((Iterable<Persona>)listPersonas);
		
		List<PersonaGet> personas = personaService.getPersonas();
		assertTrue(!personas.isEmpty());
		assertTrue(personas.size()==4);
	}
	
	@Test
	public void cuandoBuscoPersonaDadaDeAltaDevuelveLaMisma() {
		
		Optional<Persona> josePerez = Optional.of(new Persona(new Long(1),"1111111","argentina","dni","M","Jose","Perez", 20,"calle","avenida","4212-2323","1122334455"));
		Mockito.when(personaRepositoryMock.findById(new Long(1))).thenReturn(josePerez);
		
		PersonaGet result =  personaService.getPersona(new Long(1));
		assertTrue(result.getId().compareTo(josePerez.get().getId())==0);
		
	}
	
	@Test(expected = PersonaNoEncontradaException.class)
	public void cuandoBuscoPersonaNoExistenteLanzaExcepcion() {
		personaService.getPersona(new Long(10));
	}
	
	@Test(expected = PersonaDatosContactoVacioException.class)
	public void cuandoCreoUnaPersonaSinDatosDeContactoLanzaExcepcion() {
		PersonaCreateDto josePerezCreate = new PersonaCreateDto("1111111", "argentina", "dni", "M", "Jose", "Perez", 20, "", "", "", "");
		personaService.createPerson(josePerezCreate);
	}
	
	@Test
	public void cuandoCreoUnaPersonaValidaSeDioDeAltaLaMisma() {
		PersonaCreateDto josePerezCreate = new PersonaCreateDto("1111111", "argentina", "dni", "M", "Jose", "Perez", 20, "calle", "3avenida2", "4212-2323", "1122334455");
		Persona josePerez = new Persona(new Long(1),"1111111","argentina","dni","M","Jose","Perez", 20,"calle","avenida","4212-2323","1122334455");
//		Optional<Persona> opcJosePerez = Optional.of(josePerez);
		
		Mockito.when(personaRepositoryMock.findById(new Long(1))).thenReturn(Optional.of(josePerez));
		personaService.createPerson(josePerezCreate);
		assertTrue(personaService.getPersona(new Long(1)).getDocumento().equals(josePerez.getDocumento()));
	}
	
	@Test(expected = PersonaNoEncontradaException.class)
	public void cuandoBorroUnaPersonaInexistenteLanzaExcepcion() {
		personaService.deletePerson(new Long(50));
	}
	
	
	
}
