package com.api.desafio.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.api.desafio.dto.EstadisticasDto;
import com.api.desafio.entity.EstadisticaSexo;
import com.api.desafio.entity.Persona;
import com.api.desafio.repository.EstadisticasRepository;
import com.api.desafio.repository.PersonaRepository;
import com.api.desafio.service.EstadisticasService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EstadisticasServiceTest {
	
	@Autowired
	EstadisticasService estadisticasService;
	@MockBean
	EstadisticasRepository estadisticasRepository;
	@MockBean
	PersonaRepository personaRepository;
	
	EstadisticaSexo estadisticaSexo ;

	List<Persona> listPersonas;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		estadisticaSexo = new EstadisticaSexo();
		Iterable<EstadisticaSexo> estIter = new ArrayList<EstadisticaSexo>();
		Mockito.when(estadisticasRepository.getEstadisticasSexo()).thenReturn(estIter);
		
		Persona josePerez = new Persona(new Long(1),"1111111","argentina","dni","M","Jose","Perez", 20,"calle","avenida","4212-2323","1122334455");
		Persona pabloPerez = new Persona(new Long(2),"2222222","argentina","dni","M","Pablo","Perez", 24,"calle","avenida","4212-2323","1122334456");
		Persona padrePerez = new Persona(new Long(3),"3333333","paraguay","dni","M","Raul","Perez", 24,"calle","avenida","4212-2323","1122334457");
		Persona madrePerez = new Persona(new Long(4),"4444444","bolivia","dni","F","Mariz","Perez", 24,"calle","avenida","4212-2323","1122334458");

		listPersonas = new ArrayList<Persona>();
		listPersonas.add(josePerez);
		listPersonas.add(pabloPerez);
		listPersonas.add(padrePerez);
		listPersonas.add(madrePerez);
		
		Iterable<Persona> iter = listPersonas;
		Mockito.when(personaRepository.findAll()).thenReturn(iter);
	}
	
	
	@Test
	public void cuandoBuscoEstadisticasSinPersonasDevuelveDatosInicializados() {
		Iterable<Persona> it = new ArrayList<>();
		Mockito.when(personaRepository.findAll()).thenReturn(it);
		EstadisticasDto est = this.estadisticasService.getInfoEstadistica();
		assertTrue(est.getCantidad_hombres().compareTo(0)==0);
		assertTrue(est.getCantidad_mujeres().compareTo(0)==0);
		assertTrue(est.getPorcentaje_argentinos().compareTo(new Double(0))==0);
	}
	
	@Test
	public void cuandoBuscoEstadisticasDevuelveLaCantidadDeHombreYMujeresYPorcentajeCorrecto() {
		
		EstadisticaSexo estHombre = new EstadisticaSexo("M",3);
		EstadisticaSexo estMujer = new EstadisticaSexo("F",1);
		ArrayList<EstadisticaSexo> list = new ArrayList<EstadisticaSexo>();
		list.add(estMujer);
		list.add(estHombre);
		Iterable<EstadisticaSexo> estIter = list;
		
		Mockito.when(estadisticasRepository.getEstadisticasSexo()).thenReturn(estIter);
		EstadisticasDto est = this.estadisticasService.getInfoEstadistica();
		assertTrue(est.getCantidad_hombres().compareTo(3)==0);
		assertTrue(est.getCantidad_mujeres().compareTo(1)==0);
		assertTrue(est.getPorcentaje_argentinos().compareTo(new Double(50))==0);
	}
	
}
