package com.api.desafio.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.api.desafio.constants.PersonaConstants;
import com.api.desafio.entity.Persona;
import com.api.desafio.exceptions.PersonaNoEncontradaException;
import com.api.desafio.repository.PersonaRepository;
import com.api.desafio.service.RelacionesService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RelacionesServiceTest {
	
	@Autowired
	RelacionesService relacionesService;
	@MockBean
	PersonaRepository personaRepositoryMock;
	
	Persona josePerez ;
	Persona pabloPerez ;
	Persona padrePerez;
	Persona madrePerez; 
	List<Persona> listPersonas;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Persona josePerez = new Persona(new Long(1),"1111111","argentina","dni","M","Jose","Perez", 20,"calle","avenida","4212-2323","1122334455");
		Persona pabloPerez = new Persona(new Long(2),"2222222","argentina","dni","M","Pablo","Perez", 24,"calle","avenida","4212-2323","1122334456");
		Persona padrePerez = new Persona(new Long(3),"3333333","argentina","dni","M","Raul","Perez", 24,"calle","avenida","4212-2323","1122334457");
		Persona madrePerez = new Persona(new Long(4),"4444444","bolivia","dni","F","Mariz","Perez", 24,"calle","avenida","4212-2323","1122334458");
		Persona abueloPerez = new Persona(new Long(5),"555555","paraguay","dni","M","Ramon","Perez", 60,"calle","avenida","4212-2323","1122334458");
		Persona tioPerez = new Persona(new Long(6),"6666666","argentina","dni","M","Ricardo","Perez", 30,"calle","avenida","4212-2323","1122334427");
		Persona primoPerez = new Persona(new Long(7),"6666666","argentina","dni","M","Juancito","Perez", 16,"calle","avenida","4212-2323","1122334433");

		listPersonas = new ArrayList<Persona>();
		listPersonas.add(josePerez);
		listPersonas.add(pabloPerez);
		listPersonas.add(padrePerez);
		listPersonas.add(madrePerez);
		
		Mockito.when(personaRepositoryMock.findById(new Long(1))).thenReturn(Optional.of(josePerez));
		Mockito.when(personaRepositoryMock.findById(new Long(2))).thenReturn(Optional.of(pabloPerez));
		Mockito.when(personaRepositoryMock.findById(new Long(3))).thenReturn(Optional.of(padrePerez));
		Mockito.when(personaRepositoryMock.findById(new Long(4))).thenReturn(Optional.of(madrePerez));
		Mockito.when(personaRepositoryMock.findById(new Long(5))).thenReturn(Optional.of(abueloPerez));
		Mockito.when(personaRepositoryMock.findById(new Long(6))).thenReturn(Optional.of(tioPerez));
		Mockito.when(personaRepositoryMock.findById(new Long(7))).thenReturn(Optional.of(primoPerez));
		
		Mockito.when(personaRepositoryMock.save(josePerez)).thenReturn(josePerez);
		Mockito.when(personaRepositoryMock.save(josePerez)).thenReturn(josePerez);
		Mockito.when(personaRepositoryMock.save(padrePerez)).thenReturn(padrePerez);
		Mockito.when(personaRepositoryMock.save(madrePerez)).thenReturn(madrePerez);
		Mockito.when(personaRepositoryMock.save(abueloPerez)).thenReturn(abueloPerez);
		Mockito.when(personaRepositoryMock.save(tioPerez)).thenReturn(tioPerez);
		Mockito.when(personaRepositoryMock.save(primoPerez)).thenReturn(primoPerez);
		
	}
	
	
	@Test(expected = PersonaNoEncontradaException.class)
	public void cuandoSeteoElPadreDeUnHijoInexistenteDevuelveExcepcion() {
		this.relacionesService.setPadre(new Long(8), new Long(9));
	}
	
	@Test
	public void cuandoSeteoElPadreDeUnHijoComprueboQueSeHizoElCambio() {
		
		Optional<Persona> josePerezo = Optional.of(new Persona(new Long(1),"1111111","argentina","dni","M","Jose","Perez", 20,"calle","avenida","4212-2323","1122334455"));
		Optional<Persona> padrePerezo = Optional.of(new Persona(new Long(3),"2222222","argentina","dni","M","Padre","Perez", 20,"calle","avenida","4212-2323","1122334455"));

		Mockito.when(personaRepositoryMock.findById(new Long(1))).thenReturn(josePerezo);
		Mockito.when(personaRepositoryMock.findById(new Long(3))).thenReturn(padrePerezo);
		Mockito.when(personaRepositoryMock.save(josePerezo.get())).thenReturn(josePerezo.get());
		Mockito.when(personaRepositoryMock.save(padrePerezo.get())).thenReturn(padrePerezo.get());
		this.relacionesService.setPadre(new Long(1), new Long(3));
		
		assertTrue(personaRepositoryMock.findById(new Long(1)).get().equals(josePerezo.get()));
	}
	
	@Test(expected = PersonaNoEncontradaException.class)
	public void cuandoObtengoLaRelacionDeDosPersonasQueNoExisteLanzaExcepcion() {
		this.relacionesService.getRelacion(new Long(50), new Long(2));
	}
	
	@Test
	public void cuandoObtengoLaRelacionDeDosPersonasQueNoLaTieneDevuelveMsj() {
		assertTrue(this.relacionesService.getRelacion(new Long(1), new Long(2)).getMsj().equalsIgnoreCase(PersonaConstants.SIN_RELACION_MSJ));
	}
	
	@Test
	public void cuandoSeteoElMismoPadreADosHijosDevuelveRelacionHermanos() {
		
		this.relacionesService.setPadre(new Long(3), new Long(1));
		this.relacionesService.setPadre(new Long(3), new Long(2));
		
		assertTrue(this.relacionesService.getRelacion(new Long(1), new Long(2)).getMsj().equalsIgnoreCase(PersonaConstants.HERMANE_MSJ));
	}
	
	@Test
	public void cuandoSeteoElMismoPadreADosHermanosDevuelveRelacionPrimosDeLosHijos() {
		
		this.relacionesService.setPadre(new Long(3), new Long(1));
		this.relacionesService.setPadre(new Long(6), new Long(7));
		
		this.relacionesService.setPadre(new Long(5), new Long(3));
		this.relacionesService.setPadre(new Long(5), new Long(6));
		
		assertTrue(this.relacionesService.getRelacion(new Long(1), new Long(7)).getMsj().equalsIgnoreCase(PersonaConstants.PRIME_MSJ));
	}
	
	@Test
	public void cuandoSeteoElMismoPadreADosHermanosDevuelveRelacionTioDeLosHijo() {
		
		//padrePerez->josePerez
		this.relacionesService.setPadre(new Long(3), new Long(1));
		//tioPerez->primoPerez
		this.relacionesService.setPadre(new Long(6), new Long(7));
		//abueloPerez->padrePerez
		this.relacionesService.setPadre(new Long(5), new Long(3));
		//abueloPerez->tioPerez
		this.relacionesService.setPadre(new Long(5), new Long(6));
		
		//tioPerez->josePerez
		assertTrue(this.relacionesService.getRelacion(new Long(6), new Long(1)).getMsj().equalsIgnoreCase(PersonaConstants.TIE_MSJ));
	}
	
	
}
