package com.api.desafio.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.desafio.entity.EstadisticaSexo;

@Repository
public interface EstadisticasRepository extends CrudRepository<EstadisticaSexo, String> {

	@Query(value = "SELECT SEXO," + 
				   "COUNT(*) AS PERSONAS " + 
				   "FROM PERSONA " + 
				   "GROUP BY SEXO",nativeQuery = true)
	Iterable<EstadisticaSexo> getEstadisticasSexo();
	
}
