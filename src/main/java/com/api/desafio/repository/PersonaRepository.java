package com.api.desafio.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.desafio.entity.Persona;

@Repository
public interface PersonaRepository extends CrudRepository<Persona, Long> {

	Optional<Persona> findById(Long personaId);

	void deleteById(Long personaId);

	Optional<Persona> findByDocumentoAndPaisAndTipodocAndSexo(String documento, String pais, String tipodoc,
			String sexo);

}
