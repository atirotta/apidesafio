package com.api.desafio.constants;

public class PersonaConstants {

	public static final String MODEL_PERSONA = "";
	public static final String MASCULINO = "M";
	public static final String FEMENINO = "F";
	public static final String ARGENTINO = "ARGENTINA";
	public static final String OPER_OK = "Persona Creada Correctamente";
	public static final String UPD_OK = "Persona Modificada Correctamente";
	public static final String ELIMINADO_OK = "Persona Eliminada Correctamente";
	public static final String RELACION_OK = "Relacion creada Correctamente";
	public static final String NO_EXISTE_MSJ = "No existe la persona.";
	public static final String NOMBRE_MSJ = "Nombre Obligatorio";
	public static final String APELLIDO_MSJ = "Apellido Obligatorio";
	public static final String TIPDOC_MSJ = "Tipo de Documento Obligatorio";
	public static final String DOC_MSJ = "Documento Obligatorio";
	public static final String EDAD_MSJ = "Edad Obligatorio y mayor a 18 años";
	public static final String SEXO_MSJ = "Sexo Obligatorio, debe ser M o F";
	public static final String CONTACTO_MSJ = "Al menos un dato de Contacto Obligatorio";
	public static final String PAIS_MSJ = "Pais Obligatorio";
	public static final String PARSER_MSJ = "Tipo de Dato Ingresado Invalido.";
	public static final String ERROR_MSJ = "Hubo un error.";
	public static final String EXISTE_MSJ = "Ya existe la persona.";
	public static final String HERMANE_MSJ = "Herman@";
	public static final String PRIME_MSJ = "Prim@";
	public static final String TIE_MSJ = "Ti@";
	public static final String NO_EXISTE_PERSONAS_MSJ = "No hay personas cargadas en la base de datos";
	public static final String SIN_RELACION_MSJ = "No existe relacion entre las dos personas";
	public static final String CANT_HOMBRES_MSJ = "Cantidad de Hombres: ";
	public static final String CANT_MUJERES_MSJ = "Cantidad de Mujeres: ";
	public static final String ID_MSJ = "Id debe ser mayor a cero";
}
