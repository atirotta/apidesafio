package com.api.desafio.util;
import org.dozer.DozerConverter;

import com.api.desafio.dto.PersonaGet;
import com.api.desafio.entity.Persona;

public class PersonaGetConverter extends DozerConverter<Persona, PersonaGet> {

    public PersonaGetConverter() {
        super(Persona.class, PersonaGet.class);
    }

    @Override
    public PersonaGet convertTo(Persona source, PersonaGet destination) {
    	
    	if(source.getApellido() != null) destination.setApellido(source.getApellido());
    	if(source.getNombre() != null) destination.setNombre(source.getNombre());
    	if(source.getEdad() != null) destination.setEdad(source.getEdad());
    	if(source.getDomiciliolaboral() != null) destination.setDomiciliolaboral(source.getDomiciliolaboral());
    	if(source.getDomicilioparticular() != null) destination.setDomicilioparticular(source.getDomicilioparticular());
    	if(source.getTelefonomovil() != null) destination.setTelefonomovil(source.getTelefonomovil());
    	if(source.getTelefonoparticular() != null) destination.setTelefonoparticular(source.getTelefonoparticular());
    	if(source.getTipodoc() != null) destination.setTipodoc(source.getTipodoc());
    	if(source.getDocumento() != null) destination.setDocumento(source.getDocumento());
    	if(source.getSexo() != null) destination.setSexo(source.getSexo());
    	if(source.getPais() != null) destination.setPais(source.getPais());
    	if(source.getId() != null) destination.setId(source.getId());
    	if(source.getPadre() != null) destination.setPadreId(source.getPadre().getId());
    	
    	return destination;

    }


	@Override
	public Persona convertFrom(PersonaGet source, Persona destination) {
		return destination;
	}


}
