package com.api.desafio.util;
import org.dozer.DozerConverter;

import com.api.desafio.dto.PersonaUpdateDto;
import com.api.desafio.entity.Persona;

public class PersonaConverter extends DozerConverter<PersonaUpdateDto, Persona> {

    public PersonaConverter() {
        super(PersonaUpdateDto.class, Persona.class);
    }

    @Override
    public Persona convertTo(PersonaUpdateDto source, Persona destination) {
    	
    	if(source.getApellido() != null) destination.setApellido(source.getApellido());
    	if(source.getNombre() != null) destination.setNombre(source.getNombre());
    	if(source.getEdad() != null) destination.setEdad(source.getEdad());
    	if(source.getDomiciliolaboral() != null) destination.setDomiciliolaboral(source.getDomiciliolaboral());
    	if(source.getDomicilioparticular() != null) destination.setDomicilioparticular(source.getDomicilioparticular());
    	if(source.getTelefonomovil() != null) destination.setTelefonomovil(source.getTelefonomovil());
    	if(source.getTelefonoparticular() != null) destination.setTelefonoparticular(source.getTelefonoparticular());
    	if(source.getTipodoc() != null) destination.setTipodoc(source.getTipodoc());
    	if(source.getDocumento() != null) destination.setDocumento(source.getDocumento());
    	if(source.getSexo() != null) destination.setDocumento(source.getSexo());
    	if(source.getPais() != null) destination.setPais(source.getPais());
    	
    	
    	return destination;

    }


	@Override
	public PersonaUpdateDto convertFrom(Persona source, PersonaUpdateDto destination) {
		return destination;
	}


}
