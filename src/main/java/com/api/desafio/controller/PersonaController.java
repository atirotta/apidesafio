package com.api.desafio.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.desafio.dto.PersonaCreateDto;
import com.api.desafio.dto.PersonaGet;
import com.api.desafio.dto.PersonaUpdateDto;
import com.api.desafio.service.PersonaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/personas")
@Api(tags = "Personas")
public class PersonaController {
	
	@Autowired
	private PersonaService personaService;
	
	@GetMapping(value = "/")
	@ApiOperation(value = "Obtener Personas", notes = "Trae todas las personas de la base de datos")
	public ResponseEntity<List<PersonaGet>> getPersonas() {
		
		List<PersonaGet> list = this.personaService.getPersonas();
		if(list.isEmpty()) {
			return new ResponseEntity<List<PersonaGet>>(list, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<PersonaGet>>(list, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation(value = "Obtener Persona", notes = "Obtiene la persona del id en la base de datos")
    public ResponseEntity<PersonaGet> getPersona(@PathVariable("id") Long personaId) {
        return new ResponseEntity<PersonaGet>(this.personaService.getPersona(personaId), HttpStatus.OK);
    }
	
	@PostMapping
	@ApiOperation(value = "Crear Persona", notes = "Crea una persona de la base de datos")
    public ResponseEntity<String> createPersona(@Valid @RequestBody PersonaCreateDto persona) {
		this.personaService.createPerson(persona);
		return new ResponseEntity<String>(HttpStatus.CREATED);
    }
	
	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Borrar Persona", notes = "Borra una persona de la base de datos")
    public ResponseEntity<String> deletePersona(@PathVariable("id") Long personaId) {
		this.personaService.deletePerson(personaId);
		return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }
	
	@PatchMapping(value = "/{id}")
	@ApiOperation(value = "Actualiza la persona", notes = "Actualiza los campos completados de una persona en la base de datos")
    public ResponseEntity<String> updatePersona(@Valid @RequestBody PersonaUpdateDto persona, @PathVariable("id") Long personaId) {
		this.personaService.updatePersona(persona,personaId);
		return new ResponseEntity<String>(HttpStatus.OK);
    }
}
