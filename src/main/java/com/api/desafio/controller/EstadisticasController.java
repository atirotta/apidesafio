package com.api.desafio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.desafio.dto.EstadisticasDto;
import com.api.desafio.service.EstadisticasService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/estadisticas")
@Api(tags = "Estadisticas de Personas")
public class EstadisticasController {
	
	@Autowired
	private EstadisticasService estadisticasService;
	
	@GetMapping(value = "/")
	@ApiOperation(value = "Obtener Estadisticas", notes = "Devuelve la cantidad de hombres y mujeres cargadas y el porcentaje de argentinos en la base de datos ")
	public ResponseEntity<EstadisticasDto> getEstadisticas() {
		return new ResponseEntity<EstadisticasDto>(this.estadisticasService.getInfoEstadistica(), HttpStatus.OK);
	}
	
	
}
