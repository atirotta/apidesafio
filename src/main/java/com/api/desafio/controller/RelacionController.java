package com.api.desafio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.desafio.dto.RelacionDto;
import com.api.desafio.service.RelacionesService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/relaciones")
@Api(tags = "Relaciones de Personas")
public class RelacionController {
	
	@Autowired
	private RelacionesService relacionesService;
	
	@GetMapping(value = "/{id1}/{id2}")
	@ApiOperation(value = "Obtener Relacion", notes = "Devuelve la relacion que hay entre las personas recibidas por parametro")
	public ResponseEntity<RelacionDto> getEstadisticas(@PathVariable("id1") Long personaId1, @PathVariable("id2") Long personaId2) {
		return new ResponseEntity<RelacionDto>(this.relacionesService.getRelacion(personaId1, personaId2), HttpStatus.OK);
	}
	
	@PostMapping(value = "/{id1}/padre/{id2}")
	@ApiOperation(value = "Relacionar Padre", notes = "Relaciona los id's -> (id1) padre de (id2)")
    public ResponseEntity<String> setPadre(@PathVariable("id1") Long personaId1, @PathVariable("id2") Long personaId2) {
		this.relacionesService.setPadre(personaId1, personaId2);
        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
