package com.api.desafio.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EstadisticaSexo {

	@Id
	private String sexo;
	private Integer personas;
	
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Integer getPersonas() {
		return personas;
	}
	public void setPersonas(Integer personas) {
		this.personas = personas;
	}
	
	public EstadisticaSexo() {
		super();
	}
	public EstadisticaSexo(String sexo, Integer personas) {
		super();
		this.sexo = sexo;
		this.personas = personas;
	}
	
}
