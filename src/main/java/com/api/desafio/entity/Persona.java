package com.api.desafio.entity;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "PERSONA", uniqueConstraints = {@UniqueConstraint(columnNames = {"documento","tipodoc","pais","sexo"})})
public class Persona  {

	@Id
	@Column(name= "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String documento;
	private String pais;
	private String tipodoc;
	private String sexo;
	private String nombre;
	private String apellido;
	private Integer edad;
	private String domicilioparticular;
	private String domiciliolaboral;
	private String telefonoparticular;
	private String telefonomovil;
	
	@ManyToOne
	private Persona padre;
	@OneToMany(mappedBy = "id")
	private List<Persona> hijos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;	
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public String getDomicilioparticular() {
		return domicilioparticular;
	}
	public void setDomicilioparticular(String domicilioparticular) {
		this.domicilioparticular = domicilioparticular;
	}
	public String getDomiciliolaboral() {
		return domiciliolaboral;
	}
	public void setDomiciliolaboral(String domiciliolaboral) {
		this.domiciliolaboral = domiciliolaboral;
	}
	public String getTelefonoparticular() {
		return telefonoparticular;
	}
	public void setTelefonoparticular(String telefonoparticular) {
		this.telefonoparticular = telefonoparticular;
	}
	public String getTelefonomovil() {
		return telefonomovil;
	}
	public void setTelefonomovil(String telefonomovil) {
		this.telefonomovil = telefonomovil;
	}
	public Persona getPadre() {
		return padre;
	}
	public void setPadre(Persona padre) {
		this.padre = padre;
	}
	public List<Persona> getHijos() {
		return hijos;
	}
	public void setHijos(List<Persona> hijos) {
		this.hijos = hijos;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTipodoc() {
		return tipodoc;
	}
	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public Persona() {
		super();
	}
	public Persona(Long id, String documento, String pais, String tipodoc, String sexo, String nombre, String apellido,
			Integer edad, String domicilioparticular, String domiciliolaboral, String telefonoparticular,
			String telefonomovil) {
		super();
		this.id = id;
		this.documento = documento;
		this.pais = pais;
		this.tipodoc = tipodoc;
		this.sexo = sexo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.domicilioparticular = domicilioparticular;
		this.domiciliolaboral = domiciliolaboral;
		this.telefonoparticular = telefonoparticular;
		this.telefonomovil = telefonomovil;
	}
	
	
}
