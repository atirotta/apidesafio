package com.api.desafio.dto;

public class EstadisticasDto {
	
	private Integer cantidad_mujeres;
	private Integer cantidad_hombres;
	private Double porcentaje_argentinos;
	
	public Integer getCantidad_mujeres() {
		return cantidad_mujeres;
	}
	public void setCantidad_mujeres(Integer cantidad_mujeres) {
		this.cantidad_mujeres = cantidad_mujeres;
	}
	public Integer getCantidad_hombres() {
		return cantidad_hombres;
	}
	public void setCantidad_hombres(Integer cantidad_hombres) {
		this.cantidad_hombres = cantidad_hombres;
	}
	public Double getPorcentaje_argentinos() {
		return porcentaje_argentinos;
	}
	public void setPorcentaje_argentinos(Double porcentaje_argentinos) {
		this.porcentaje_argentinos = porcentaje_argentinos;
	}
	public EstadisticasDto() {
		super();
		this.cantidad_hombres=0;
		this.cantidad_mujeres=0;
	}
	
}
