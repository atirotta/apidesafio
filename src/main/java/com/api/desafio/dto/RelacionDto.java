package com.api.desafio.dto;

public class RelacionDto {
	
	private String msj;

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public RelacionDto(String msj) {
		super();
		this.msj = msj;
	}

	public RelacionDto() {
		super();
	}
	
	

}
