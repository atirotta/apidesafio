package com.api.desafio.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import com.api.desafio.constants.PersonaConstants;

public class PersonaUpdateDto  {

	@NotBlank(message = PersonaConstants.DOC_MSJ)
	private String documento;
	@NotBlank(message = PersonaConstants.PAIS_MSJ)
	private String pais;
	@NotBlank(message = PersonaConstants.TIPDOC_MSJ)
	private String tipodoc;
	@Pattern(regexp = "[MmFf]{1}", message = PersonaConstants.SEXO_MSJ)
	private String sexo;
	private String nombre;
	private String apellido;
	@Min(value = 18, message = PersonaConstants.EDAD_MSJ)
	@Positive(message = PersonaConstants.EDAD_MSJ)
	private Integer edad;
	private String domicilioparticular;
	private String domiciliolaboral;
	private String telefonoparticular;
	private String telefonomovil;
	
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTipodoc() {
		return tipodoc;
	}
	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public String getDomicilioparticular() {
		return domicilioparticular;
	}
	public void setDomicilioparticular(String domicilioparticular) {
		this.domicilioparticular = domicilioparticular;
	}
	public String getDomiciliolaboral() {
		return domiciliolaboral;
	}
	public void setDomiciliolaboral(String domiciliolaboral) {
		this.domiciliolaboral = domiciliolaboral;
	}
	public String getTelefonoparticular() {
		return telefonoparticular;
	}
	public void setTelefonoparticular(String telefonoparticular) {
		this.telefonoparticular = telefonoparticular;
	}
	public String getTelefonomovil() {
		return telefonomovil;
	}
	public void setTelefonomovil(String telefonomovil) {
		this.telefonomovil = telefonomovil;
	}
	public PersonaUpdateDto() {
		super();
	}
	
}
