package com.api.desafio.dto;

public class ErrorDto {

	private String msj;

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public ErrorDto() {
		super();
	}

	public ErrorDto(String msj) {
		super();
		this.msj = msj;
	}
	
	
}
