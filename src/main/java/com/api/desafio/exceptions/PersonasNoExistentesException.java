package com.api.desafio.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No hay personas cargadas")
public class PersonasNoExistentesException extends PersonaException {

}
