package com.api.desafio.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Errores en la entrada de datos")
public class PersonaValidateException extends PersonaException {

	
}
