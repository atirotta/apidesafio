package com.api.desafio.exceptions;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.json.JsonParseException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.api.desafio.constants.PersonaConstants;
import com.api.desafio.dto.ErrorDto;

@ControllerAdvice
public class ExceptionPersonaController {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Map<String, String>> handleExceptionPersona(MethodArgumentNotValidException e) {
		
		Map<String, String> errors = new HashMap<>();
	    e.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return new ResponseEntity<Map<String,String>>(errors,HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(JsonParseException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorDto> handleExceptionParserPersona(JsonParseException e) {
	    return new ResponseEntity<ErrorDto>(new ErrorDto(PersonaConstants.PARSER_MSJ),HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(PersonaDatosContactoVacioException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorDto> handleExceptionDatosContacto(PersonaDatosContactoVacioException e) {
	    return new ResponseEntity<ErrorDto>(new ErrorDto(PersonaConstants.CONTACTO_MSJ),HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(PersonaExistenteException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorDto> handleExceptionPersonaExistente(PersonaExistenteException e) {
	    return new ResponseEntity<ErrorDto>(new ErrorDto(PersonaConstants.EXISTE_MSJ),HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	@ExceptionHandler(PersonaNoEncontradaException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorDto> handleExceptionPersonaExistente(PersonaNoEncontradaException e) {
	    return new ResponseEntity<ErrorDto>(new ErrorDto(PersonaConstants.NO_EXISTE_MSJ),HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	@ExceptionHandler(PersonasNoExistentesException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorDto> handleExceptionPersonaExistente(PersonasNoExistentesException e) {
	    return new ResponseEntity<ErrorDto>(new ErrorDto(PersonaConstants.NO_EXISTE_PERSONAS_MSJ),HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorDto> handleExceptionPersonaExistente(DataIntegrityViolationException e) {
	    return new ResponseEntity<ErrorDto>(new ErrorDto(PersonaConstants.EXISTE_MSJ),HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<ErrorDto> handleExceptionGeneral(Exception e) {
		return new ResponseEntity<ErrorDto>(new ErrorDto(PersonaConstants.ERROR_MSJ),HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
