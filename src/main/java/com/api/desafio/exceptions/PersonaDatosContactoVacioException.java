package com.api.desafio.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.api.desafio.constants.PersonaConstants;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = PersonaConstants.CONTACTO_MSJ)
public class PersonaDatosContactoVacioException extends PersonaException {

}
