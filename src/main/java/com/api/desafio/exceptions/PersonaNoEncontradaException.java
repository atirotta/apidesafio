package com.api.desafio.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Persona no encontrada")
public class PersonaNoEncontradaException extends PersonaException {

}
