package com.api.desafio.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.api.desafio.dto.PersonaCreateDto;
import com.api.desafio.dto.PersonaGet;
import com.api.desafio.dto.PersonaUpdateDto;
import com.api.desafio.entity.Persona;
import com.api.desafio.exceptions.PersonaDatosContactoVacioException;
import com.api.desafio.exceptions.PersonaNoEncontradaException;
import com.api.desafio.exceptions.PersonasNoExistentesException;
import com.api.desafio.repository.PersonaRepository;

@Service
public class PersonaService {

	@Autowired
	private PersonaRepository personaRepository;
	
	public List<PersonaGet> getPersonas() {
		Iterable<Persona> personas = this.personaRepository.findAll();
		if(personas == null || ((List<Persona>) personas).isEmpty()) {
			throw new PersonasNoExistentesException();
		}
		
		DozerBeanMapper mapper = new DozerBeanMapper();
		mapper.setMappingFiles(Arrays.asList("dozer-config.xml"));
		List<PersonaGet> listPersonas = new ArrayList<>();
		for (Persona persona : personas) {
			PersonaGet personDto = new PersonaGet();
			mapper.map(persona, personDto);
			listPersonas.add(personDto);
		}
		return listPersonas;
	}

	public PersonaGet getPersona(Long personaId) {
		Optional<Persona> persona = this.personaRepository.findById(personaId);
		if(!persona.isPresent()) {
			throw new PersonaNoEncontradaException();
		}
		DozerBeanMapper mapper = new DozerBeanMapper();
		mapper.setMappingFiles(Arrays.asList("dozer-config.xml"));
		PersonaGet personDto = new PersonaGet();
		mapper.map(persona.get(), personDto);
		
		return personDto;
	}

	@Transactional
	public void createPerson(PersonaCreateDto persona) {
		
		this.validateDatosContacto(persona);
		
		DozerBeanMapper mapper = new DozerBeanMapper();
		Persona personaEntity = mapper.map(persona, Persona.class);
		personaEntity.setId(new Long(0));
		this.personaRepository.save(personaEntity);
	}

	private void validateDatosContacto(PersonaCreateDto persona) {
		if(StringUtils.isEmpty(persona.getDomiciliolaboral()) &&
		   StringUtils.isEmpty(persona.getDomicilioparticular()) &&	
		   StringUtils.isEmpty(persona.getTelefonomovil()) &&
		   StringUtils.isEmpty(persona.getTelefonoparticular()) ){
			   throw new PersonaDatosContactoVacioException();
		   }
	}

	@Transactional
	public void deletePerson(Long personaId) {
		Optional<Persona> persona = this.personaRepository.findById(personaId);
		if(!persona.isPresent()) {
			throw new PersonaNoEncontradaException();
		}
		this.personaRepository.deleteById(personaId);
	}

	@Transactional
	public void updatePersona(@Valid PersonaUpdateDto personaRequest, Long personaId) {
		Optional<Persona> opcPersona = this.personaRepository.findById(personaId);
		if(!opcPersona.isPresent()){
			throw new PersonaNoEncontradaException();
		}
		Persona persona = opcPersona.get();
		DozerBeanMapper mapper = new DozerBeanMapper();
		mapper.setMappingFiles(Arrays.asList("dozer-config.xml"));
		mapper.map(personaRequest, persona);
		this.personaRepository.save(persona);
	}
}
