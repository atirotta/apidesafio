package com.api.desafio.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.api.desafio.constants.PersonaConstants;
import com.api.desafio.dto.RelacionDto;
import com.api.desafio.entity.Persona;
import com.api.desafio.exceptions.PersonaNoEncontradaException;
import com.api.desafio.repository.PersonaRepository;

@Service
public class RelacionesService {

	@Autowired
	private PersonaRepository personaRepository;
	
	@Transactional
	public void setPadre(Long personaId1, Long personaId2) {
		
		Optional<Persona> hijo = this.personaRepository.findById(personaId2);
		Optional<Persona> padre = this.personaRepository.findById(personaId1);
		
		if(!hijo.isPresent() || !padre.isPresent()) {
			throw new PersonaNoEncontradaException();
		}
		hijo.get().setPadre(padre.get());
		this.personaRepository.save(hijo.get());
	}

	public RelacionDto getRelacion( Long personaId1,Long personaId2) {
		
		Optional<Persona> personaUno = this.personaRepository.findById(personaId1);
		Optional<Persona> personaDos = this.personaRepository.findById(personaId2);
		
		if(!personaUno.isPresent()  || !personaDos.isPresent()) {
			throw new PersonaNoEncontradaException();
		}
		
		Persona padreUno = personaUno.get().getPadre();
		Persona padreDos = personaDos.get().getPadre();
		
		if(padreUno == null || padreDos == null) {
			 return new RelacionDto(PersonaConstants.SIN_RELACION_MSJ);
		}
		if(padreUno.equals(padreDos)) {
			return new RelacionDto(PersonaConstants.HERMANE_MSJ);
		}
		
		if(this.getRelacion(padreUno.getId(), padreDos.getId()).getMsj().equalsIgnoreCase(PersonaConstants.HERMANE_MSJ)){
			return new RelacionDto(PersonaConstants.PRIME_MSJ);
		}
		
		if(this.getRelacion(personaId1, personaDos.get().getPadre().getId()).getMsj().equalsIgnoreCase(PersonaConstants.HERMANE_MSJ)){
			return new RelacionDto(PersonaConstants.TIE_MSJ);
		}
		
		return new RelacionDto(PersonaConstants.SIN_RELACION_MSJ);
	}
}
