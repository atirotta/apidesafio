package com.api.desafio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.desafio.constants.PersonaConstants;
import com.api.desafio.dto.EstadisticasDto;
import com.api.desafio.entity.EstadisticaSexo;
import com.api.desafio.entity.Persona;
import com.api.desafio.repository.EstadisticasRepository;
import com.api.desafio.repository.PersonaRepository;

@Service
public class EstadisticasService {

	@Autowired
	private EstadisticasRepository estadisticasRepository;
	
	@Autowired
	private PersonaRepository personaRepository;
	
	public EstadisticasDto getInfoEstadistica() {
		
		EstadisticasDto estadistica = new EstadisticasDto();
		
		Iterable<EstadisticaSexo> estadisticaSexo =this.estadisticasRepository.getEstadisticasSexo();
		for (EstadisticaSexo est : estadisticaSexo) {
			if(est.getSexo().equalsIgnoreCase(PersonaConstants.FEMENINO)) {
				estadistica.setCantidad_mujeres(est.getPersonas());
			}else if(est.getSexo().equalsIgnoreCase(PersonaConstants.MASCULINO)){
				estadistica.setCantidad_hombres(est.getPersonas());
			}
		
		}
		estadistica.setPorcentaje_argentinos(this.getPorcentajeArg());
		return estadistica;
	}

	private double getPorcentajeArg() {
		
		double cantArg = 0;
		double cantTotal = 0;
		Iterable<Persona> personas = this.personaRepository.findAll();
		if(personas == null) {
			return 0;
		}
		
		for (Persona persona : personas) {
			if(persona.getPais().equalsIgnoreCase(PersonaConstants.ARGENTINO)) {
				cantArg++;
			}
			cantTotal++;
		}
		
		if(cantTotal > 0) {
			return (cantArg*100)/cantTotal;
		}
		return 0;
	}

	
}
